package suitebot2;

import suitebot2.ai.BotAi;
import suitebot2.json.JsonUtil;
import suitebot2.server.SimpleRequestHandler;

public class BotRequestHandler implements SimpleRequestHandler
{
	private final BotAi botAi;
	private boolean suppressErrorLogging = false;

	public BotRequestHandler(BotAi botAi)
	{
		this.botAi = botAi;
	}

	@Override
	public String processRequest(String request)
	{
		try
		{
			return processRequestInternal(request);
		}
		catch (Exception e)
		{
			if (!suppressErrorLogging)
			{
				System.err.println("error while processing the request: " + request);
				e.printStackTrace();
			}

			return e.toString();
		}
	}

	private String processRequestInternal(String request)
	{
		switch (JsonUtil.decodeMessageType(request))
		{
			case SETUP:
				return botAi.initializeAndMakeMove(JsonUtil.decodeSetupMessage(request));
			case MOVES:
				return botAi.makeMove(JsonUtil.decodeMovesMessage(request));
			default:
				throw new IllegalArgumentException("unexpected request: " + request);
		}
	}

	public void setSuppressErrorLogging(boolean suppressErrorLogging)
	{
		this.suppressErrorLogging = suppressErrorLogging;
	}
}
